package com.example.httpconection_lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.httpconection_lab2.Task.Task_POST_V;

public class VLapPhuongActivity extends AppCompatActivity {
    private EditText edtEdge;
    private Button btnSend;
    private TextView tvResult;
    String strEdge;
    public static final String SERVER_NAME = "http://192.168.1.128/AndroidNetworking/student_POST_VLapPhuong.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_v_lap_phuong);
        anhxa();
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strEdge = edtEdge.getText().toString();
                Task_POST_V task_post_v = new Task_POST_V(VLapPhuongActivity.this,tvResult);
                task_post_v.execute(strEdge);
            }
        });
    }
    void anhxa(){
        edtEdge = findViewById(R.id.edtEdge);
        btnSend = findViewById(R.id.btSendV);
        tvResult = findViewById(R.id.tvResultV);
    }
}