package com.example.httpconection_lab2.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.example.httpconection_lab2.PostActivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Task_Post_L_W extends AsyncTask<Void, Void, Void> {
    String duongdan = PostActivity.SERVER_NAME;
    Context context;
    String strWidth, strLength, strResult;
    TextView tvResult;
    ProgressDialog progressDialog;

    public Task_Post_L_W(Context context, String strWidth, String strLength, TextView tvResult){
        this.context = context;
        this.strWidth = strWidth;
        this.strLength = strLength;
        this.tvResult = tvResult;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage( "Calculating..." );
        progressDialog.setIndeterminate( false );
        progressDialog.setCancelable( false );
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL(duongdan);
            String param = "chieurong=" + URLEncoder.encode( strWidth, "UTF-8") + "&chieudai=" + URLEncoder.encode( strLength, "UTF-8");
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setDoOutput( true );
            urlConnection.setRequestMethod( "POST" );
            urlConnection.setFixedLengthStreamingMode( param.getBytes().length );
            urlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded" );

            PrintWriter printWriter = new PrintWriter(urlConnection.getOutputStream());
            printWriter.print( param );
            printWriter.close();

            String line = "";
            BufferedReader bfr = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer sb = new StringBuffer("");
            while ((line = bfr.readLine()) != null){
                sb.append( line );
            }
            strResult = sb.toString();
            urlConnection.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        tvResult.setText( strResult );
        Log.d( "", "onPostExecute: " + duongdan );
        Log.d( "", "onPostExecute: " + strResult );
    }
}
