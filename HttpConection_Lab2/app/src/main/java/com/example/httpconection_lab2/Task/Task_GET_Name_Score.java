package com.example.httpconection_lab2.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.example.httpconection_lab2.MainActivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Task_GET_Name_Score extends AsyncTask<Void, Void, Void> {
    String url = MainActivity.SERVER_NAME;
    TextView tvResult;
    String strName, strScore, strResult;
    ProgressDialog progressDialog;
    Context context;

    public Task_GET_Name_Score(TextView tvResult, String strName, String strScore, Context context) {
        this.tvResult = tvResult;
        this.strName = strName;
        this.strScore = strScore;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage( "Sending..." );
        progressDialog.setIndeterminate( false );
        progressDialog.setCancelable( false );
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        url += "?name=" + this.strName + "&score=" + this.strScore;
        Log.d( "TAG", "" + url );
        try {
            URL urls = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) urls.openConnection();
            BufferedReader bfr = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            StringBuffer sb = new StringBuffer();
            while ((line = bfr.readLine()) != null){
                sb.append(line);
            }
            strResult = sb.toString();
            urlConnection.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        tvResult.setText( strResult );
    }
}
