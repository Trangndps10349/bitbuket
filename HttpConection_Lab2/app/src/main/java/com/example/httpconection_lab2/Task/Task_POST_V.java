package com.example.httpconection_lab2.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import com.example.httpconection_lab2.VLapPhuongActivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Task_POST_V extends AsyncTask<String, Void, Void> {
    String duongdan = VLapPhuongActivity.SERVER_NAME;
    Context context;
    TextView tvResult;
    ProgressDialog progressDialog;
    String strResult;

    public Task_POST_V(Context context, TextView tvResult) {
        this.context = context;
        this.tvResult = tvResult;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage( "Calculating..." );
        progressDialog.setIndeterminate( false );
        progressDialog.setCancelable( false );
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            URL url = new URL(duongdan);
            String param = "canh=" + URLEncoder.encode( strings[0].toString(), "UTF-8");
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setDoOutput( true );
            urlConnection.setRequestMethod( "POST" );
            urlConnection.setFixedLengthStreamingMode( param.getBytes().length );
            urlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded" );

            PrintWriter printWriter = new PrintWriter(urlConnection.getOutputStream());
            printWriter.print( param );
            printWriter.close();

            String line = "";
            BufferedReader bfr = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer sb = new StringBuffer("");
            while ((line = bfr.readLine()) != null){
                sb.append( line );
            }
            strResult = sb.toString();
            urlConnection.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        tvResult.setText(strResult);
        Log.d( "", "onPostExecute: " + duongdan );
        Log.d( "", "onPostExecute: " + strResult );
    }
}
