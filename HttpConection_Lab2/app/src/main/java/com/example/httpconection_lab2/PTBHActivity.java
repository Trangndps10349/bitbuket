package com.example.httpconection_lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.httpconection_lab2.Task.Task_POST_Ptbh;

public class PTBHActivity extends AppCompatActivity {
    private EditText edt_a, edt_b, edt_c;
    private Button bt_send;
    private TextView tvResult;
    String strA, strB, strC;
    public static final String SERVER_NAME = "http://192.168.1.128/AndroidNetworking/student_POST_PTB2.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_t_b_h);
        anhxa();
        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strA = edt_a.getText().toString();
                strB = edt_b.getText().toString();
                strC = edt_c.getText().toString();
                Task_POST_Ptbh task_post_ptbh = new Task_POST_Ptbh(PTBHActivity.this,tvResult);
                task_post_ptbh.execute(strA,strB,strC);
            }
        });
    }
    void anhxa(){
        edt_a = findViewById(R.id.edt_a);
        edt_b = findViewById(R.id.edt_b);
        edt_c = findViewById(R.id.edt_c);
        bt_send = findViewById(R.id.bt_send2_4);
        tvResult = findViewById(R.id.tv_result2_4);
    }
}