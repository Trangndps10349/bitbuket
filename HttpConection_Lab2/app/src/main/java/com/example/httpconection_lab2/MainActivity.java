package com.example.httpconection_lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.httpconection_lab2.Task.Task_GET_Name_Score;

public class MainActivity extends AppCompatActivity {
    public static final String SERVER_NAME = "http://10.82.158.130/AndroidNetworking/student_GET.php";
    EditText edtName, edtScore;
    Button btnSendGET, btnBai2, btnBai3, btnBai4;
    TextView tvResultGet;
    String strName,strScore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        btnSendGET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNameAndScore();
            }
        });
        btnBai2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,PostActivity.class);
                startActivity(intent);
            }
        });
        btnBai3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,VLapPhuongActivity.class);
                startActivity(intent);
            }
        });
        btnBai4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PTBHActivity.class);
                startActivity(intent);
            }
        });

    }
    public void anhXa(){
        edtName = findViewById(R.id.edtGetName);
        edtScore = findViewById(R.id.edtGetScore);
        btnSendGET = findViewById(R.id.btnSendGet);
        btnBai2 = findViewById(R.id.btnB2);
        tvResultGet = findViewById(R.id.tvResultGet);
        btnBai3 = findViewById(R.id.btnB3);
        btnBai4 = findViewById(R.id.btnB4);
    }
    public void getNameAndScore(){
        strName = edtName.getText().toString();
        strScore = edtScore.getText().toString();
        Task_GET_Name_Score task_get_name_score = new Task_GET_Name_Score(tvResultGet,strName,strScore,MainActivity.this);
        task_get_name_score.execute();
    }
}