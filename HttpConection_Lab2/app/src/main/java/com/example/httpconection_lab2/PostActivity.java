package com.example.httpconection_lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.httpconection_lab2.Task.Task_Post_L_W;

public class PostActivity extends AppCompatActivity {
    public static final String SERVER_NAME = "http://192.168.1.128/AndroidNetworking/student_POST.php";
    EditText edtWidth, edtLength;
    Button btnSendPost;
    TextView tvResultPost;
    String strW,strL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        anhXa();
        btnSendPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                post();
            }
        });
    }
    public void anhXa(){
        edtWidth = findViewById(R.id.edtPostD);
        edtLength = findViewById(R.id.edtPostR);
        btnSendPost = findViewById(R.id.btnSendPost);
        tvResultPost = findViewById(R.id.tvResultPost);
    }
    public void post(){
        strL = edtLength.getText().toString();
        strW = edtWidth.getText().toString();
        Task_Post_L_W TASKPOST = new Task_Post_L_W(PostActivity.this,strW,strL,tvResultPost);
        TASKPOST.execute();
    }

}