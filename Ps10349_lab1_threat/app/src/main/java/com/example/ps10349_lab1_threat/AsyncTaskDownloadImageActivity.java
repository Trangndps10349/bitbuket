package com.example.ps10349_lab1_threat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URL;

public class AsyncTaskDownloadImageActivity extends AppCompatActivity implements View.OnClickListener,Listener {
    private TextView textView;
    private Button button;
    private Button buttonIntent;
    private ImageView imageView;
    public static final String URL_IMAGE = "http://i64.tinypic.com/28vaq8k.png";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task_download_image);
        anhXa();
        button.setOnClickListener(this);
        buttonIntent.setOnClickListener(this);
    }
    public void anhXa() {
        imageView = findViewById(R.id.imageView1);
        textView = findViewById(R.id.textView1);
        button = findViewById(R.id.button1);
        buttonIntent = findViewById(R.id.buttonIntent2);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button1:
                new LoadImageTask(this,this).execute(URL_IMAGE);
                break;
            case R.id.buttonIntent2:
                Intent intent = new Intent(AsyncTaskDownloadImageActivity.this,SleepTimeInSeconds.class);
                startActivity(intent);
        }
    }

    @Override
    public void onImageLoaded(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
        textView.setText("Image Dowloaded");
    }

    @Override
    public void onError() {
        textView.setText("Error");
    }
}