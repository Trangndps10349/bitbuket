package com.example.ps10349_lab1_threat;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    TextView textView;
    Button button;
    Button buttonIntend;
    ProgressDialog progressDialog;
    Bitmap bitmap = null;
    String url="http://i64.tinypic.com/28vaq8k.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog  =  ProgressDialog.show(MainActivity.this,"Downloading Image","Waiting..");
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        bitmap = loadImageFromNetwork(url);
                        Message message = messageHandler.obtainMessage();
                        Bundle bundle = new Bundle();
                        String mess = "Done!!";
                        bundle.putString("Mess",mess);
                        message.setData(bundle);
                        messageHandler.sendMessage(message);
                    }
                };
                Thread thread = new Thread(runnable);
                thread.start();
            }
        });
        buttonIntend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,AsyncTaskDownloadImageActivity.class);
                startActivity(intent);
            }
        });
    }

    public void anhXa() {
        imageView = findViewById(R.id.imageView);
        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button);
        buttonIntend = findViewById(R.id.buttonIntend);
    }
    private Handler messageHandler = new Handler(){
        public void  handleMessage(Message msg){
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String message = bundle.getString("message");
            textView.setText(message);
            imageView.setImageBitmap(bitmap);
            progressDialog.dismiss();
        }
    };

    private Bitmap loadImageFromNetwork(String link) {
        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            Bitmap bmp = BitmapFactory.decodeStream(inputStream);
            return bmp;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}