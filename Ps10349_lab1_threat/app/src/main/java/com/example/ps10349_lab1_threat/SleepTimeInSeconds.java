package com.example.ps10349_lab1_threat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SleepTimeInSeconds extends AppCompatActivity implements View.OnClickListener {
    private EditText editText;
    private Button button;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep_time_in_seconds);
        anhXa();

    }

    public void anhXa(){
        editText = findViewById(R.id.editText);
        button = findViewById(R.id.buttonRun);
        textView = findViewById(R.id.textViewResutl);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
    switch (view.getId()){
        case R.id.buttonRun:
            AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner(textView,editText,this);
            String sleepTime = editText.getText().toString();
            asyncTaskRunner.execute(sleepTime);
            break;
    }
    }
}