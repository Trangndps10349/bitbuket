package com.example.ps10349_lab1_threat;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.TextView;

public class AsyncTaskRunner extends AsyncTask<String, String,String> {
    String resp;
    ProgressDialog dialog;
    TextView textView;
    EditText editText;
    Context context;

    public AsyncTaskRunner(TextView textView, EditText editText, Context context) {
        this.textView = textView;
        this.editText = editText;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = ProgressDialog.show(context,"ProcessDialog","Waiting for "+ editText.getText().toString()+" seconds");
    }

    @Override
    protected String doInBackground(String... strings) {
        publishProgress("Sleeping...");
        try {
            int time = Integer.parseInt(strings[0])*1000;
            Thread.sleep(time);
            resp = "sleep for " + strings[0] + " seconds";
        } catch (InterruptedException e) {
            e.printStackTrace();
            resp = e.getMessage();
        }
        return resp;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (dialog.isShowing()){
            dialog.dismiss();
        }textView.setText(s);
    }
}
