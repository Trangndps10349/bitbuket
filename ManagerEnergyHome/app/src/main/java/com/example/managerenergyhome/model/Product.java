package com.example.managerenergyhome.model;

public class Product {
    private int id;
    private String email;
    private String name;
    private String price;
    private String power;
    private String number;
    private String createDay;

    public Product() {
    }

    public Product(int id, String email, String name, String price, String power, String number,String createDate) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.price = price;
        this.power = power;
        this.number = number;
        this.createDay = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCreateDay() {
        return createDay;
    }

    public void setCreateDay(String createDay) {
        this.createDay = createDay;
    }
}
