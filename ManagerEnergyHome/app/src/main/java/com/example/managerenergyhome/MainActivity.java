package com.example.managerenergyhome;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.managerenergyhome.fragment.AboutFragment;
import com.example.managerenergyhome.fragment.HomeFragment;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    String url = "http://10.0.2.2/asm/insertProduct.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomAppBar bottomAppBar = findViewById(R.id.bottomAppBar);
        bottomAppBar.setOnMenuItemClickListener(navListener);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new HomeFragment())
                .commit();

        FloatingActionButton btnFAB = findViewById(R.id.btnFloatActionAdd);
        btnFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogAddProduct();
            }
        });
    }
    private void dialogAddProduct() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_add_product, null);

        final TextInputEditText edtNameProduct = v.findViewById(R.id.edtInputNameProduct);
        final TextInputEditText edtPrice = v.findViewById(R.id.edtInputPrice);
        final TextInputEditText edtPower = v.findViewById(R.id.edtInputPower);
        final TextInputEditText edtNumBer = v.findViewById(R.id.edtInputNumber);

        builder.setView(v);

        builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Bundle extras = getIntent().getExtras();
                final String strEmail;
                final String strName = edtNameProduct.getText().toString();
                final String strPrice = edtPrice.getText().toString();
                final String strPower = edtPower.getText().toString();
                final String strNumber = edtNumBer.getText().toString();


                if (strName == null){
                    Toast.makeText(MainActivity.this, "Name is emty", Toast.LENGTH_SHORT).show();
                }if (strPrice == null){
                    Toast.makeText(MainActivity.this, "Price is emty", Toast.LENGTH_SHORT).show();
                }if(strPower == null){
                    Toast.makeText(MainActivity.this, "Power is emty", Toast.LENGTH_SHORT).show();
                }if (strNumber == null){
                    Toast.makeText(MainActivity.this, "Number is emty", Toast.LENGTH_SHORT).show();
                }if (extras == null){
                    strEmail = null;
                }else {
                    strEmail = extras.getString("email");
                    final StringRequest request = new StringRequest(Request.Method.POST,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    switch (response) {
                                        case "403":
                                            Toast.makeText(MainActivity.this, "no data", Toast.LENGTH_SHORT).show();
                                            break;
                                        case "400":
                                            Toast.makeText(MainActivity.this, " error", Toast.LENGTH_SHORT).show();
                                            break;
                                        case "200":
                                            Toast.makeText(MainActivity.this, "insert product successfully", Toast.LENGTH_SHORT).show();
                                            break;
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MainActivity.this, error.getMessage().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    ) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("email",strEmail);
                            params.put("name_product",strName);
                            params.put("price",strPrice);
                            params.put("power_consumption",strPower);
                            params.put("number",strNumber);
                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                    requestQueue.add(request);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private BottomAppBar.OnMenuItemClickListener navListener = new BottomAppBar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()){
                case R.id.menu1:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.menu2:
                    selectedFragment = new AboutFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, selectedFragment)
                    .commit();
            return true;
        }
    };
}