package com.example.managerenergyhome.adapter;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.managerenergyhome.R;
import com.example.managerenergyhome.model.Product;

import java.util.List;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.ViewHolder> {
    Context context;
    List<Product> itemListProduct1;
    private int position = -1;

    public AdapterProduct(Context context, List<Product> itemListProduct1) {
        this.context = context;
        this.itemListProduct1 = itemListProduct1;
    }


    @NonNull
    @Override
    public AdapterProduct.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }
    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterProduct.ViewHolder holder, int position) {

        holder.tvName.setText(itemListProduct1.get(position).getName());
        holder.tvPrice.setText(itemListProduct1.get(position).getPrice());
        holder.tvPower.setText(itemListProduct1.get(position).getPower());
        holder.tvNumber.setText(itemListProduct1.get(position).getNumber());

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemListProduct1.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        ImageView imageViewProduct;
        TextView tvName,tvPrice,tvPower,tvNumber;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewProduct = itemView.findViewById(R.id.imgImageProduct);
            tvName = itemView.findViewById(R.id.itemTvName);
            tvPrice = itemView.findViewById(R.id.itemTvPrice);
            tvPower = itemView.findViewById(R.id.itemTvPower);
            tvNumber = itemView.findViewById(R.id.itemTvNumber);

            itemView.setOnCreateContextMenuListener(this);



        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view,
                                        ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(Menu.NONE, R.id.edit_product,
                    Menu.NONE, R.string.edit);
            contextMenu.add(Menu.NONE,R.id.delete_prodluct,
                    Menu.NONE,R.string.delete);
        }
    }
}
