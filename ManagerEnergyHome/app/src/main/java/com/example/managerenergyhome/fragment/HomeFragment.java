package com.example.managerenergyhome.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.managerenergyhome.MainActivity;
import com.example.managerenergyhome.R;
import com.example.managerenergyhome.adapter.AdapterProduct;
import com.example.managerenergyhome.model.Product;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment {

    public HomeFragment(){}
    RecyclerView recyclerView;
    List<Product> itemListProduct;
    List<Product> itemListProduct1;
    AdapterProduct adapterProduct;
    String url = "http://10.0.2.2/asm/getAllProduct.php";
    String urlEdit = "http://10.0.2.2/asm/editProduct.php";
    String urlDelete = "http://10.0.2.2/asm/deleteProduct.php";
    SharedPreferences sharedPreferences;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container,false);

        recyclerView = view.findViewById(R.id.recycleviewProduct);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        //initData();
        sharedPreferences = getContext().getSharedPreferences("myfile", Context.MODE_PRIVATE);
        Boolean dataSave = sharedPreferences.getBoolean("save_information", false);
        if (dataSave) {
            String txtEmail = sharedPreferences.getString("email", "");

            //Log.d("txtEmail", txtEmail);
            retrieveData(txtEmail);
        }else {
            adapterProduct = new AdapterProduct(getContext(),initData());
            recyclerView.setAdapter(adapterProduct);
            adapterProduct.notifyDataSetChanged();
        }


        return view;
    }

    private List<Product> initData() {
        itemListProduct = new ArrayList<>();
        itemListProduct.add(new Product(1,"trangmap@gmail.com","May Quat", "20$", "40W","4","10/04/2000"));
        itemListProduct.add(new Product(2,"trangmap@gmail.com","Tu lanh", "40$", "85W","1","10/04/2000"));
        itemListProduct.add(new Product(3,"trangmap@gmail.com","Ti vi", "60$", "40W","2","10/04/2000"));
        itemListProduct.add(new Product(4,"trangmap@gmail.com","May giac", "30$", "20W","1","10/04/2000"));
        itemListProduct.add(new Product(5,"trangmap@gmail.com","Dieu hoa", "50$", "80W","2","10/04/2000"));
        return itemListProduct;
    }

    private void retrieveData(final String txtEmail){
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                itemListProduct1 = new ArrayList<>();
                itemListProduct1.clear();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i< jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String _productId = jsonObject.getString("id_product");
                        String _email = jsonObject.getString("email");
                        String _productName = jsonObject.getString("name_product");
                        String _price = jsonObject.getString("price");
                        String _powerConsumption = jsonObject.getString("power_consumption");
                        String _number = jsonObject.getString("number");
                        String _creatDate = jsonObject.getString("create_date");
                        itemListProduct1.add(new Product(Integer.parseInt(_productId), _email, _productName, _price, _powerConsumption, _number, _creatDate));
                    }
                    adapterProduct = new AdapterProduct(getContext(), itemListProduct1);
                    recyclerView.setAdapter(adapterProduct);
                    adapterProduct.notifyDataSetChanged();




                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", txtEmail);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(request);


    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        final int position;
        try{
            position = ((AdapterProduct) recyclerView.getAdapter()).getPosition();
        }catch (Exception e){
            return  super.onContextItemSelected(item);

        }
        switch (item.getItemId()){
            case R.id.edit_product:
            final Product product = itemListProduct1.get(position);
                final AlertDialog.Builder builder = new AlertDialog.Builder(getLayoutInflater().getContext());
                final LayoutInflater inflater = getActivity().getLayoutInflater();
                View v = inflater.inflate(R.layout.dialog_add_product, null);

                final TextInputEditText edtNameProduct = v.findViewById(R.id.edtInputNameProduct);
                final TextInputEditText edtPrice = v.findViewById(R.id.edtInputPrice);
                final TextInputEditText edtPower = v.findViewById(R.id.edtInputPower);
                final TextInputEditText edtNumBer = v.findViewById(R.id.edtInputNumber);
                if (product != null){
                    edtNameProduct.setText(product.getName());
                    edtPrice.setText(product.getPrice());
                    edtPower.setText(product.getPower());
                    edtNumBer.setText(product.getNumber());
                }else {
                    Toast.makeText(getContext(), "Data emty", Toast.LENGTH_SHORT).show();
                }
                builder.setView(v);
                builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        final String strName = edtNameProduct.getText().toString();
                        final String strPrice = edtPrice.getText().toString();
                        final String strPower = edtPower.getText().toString();
                        final String strNumber = edtNumBer.getText().toString();
                        final String id = String.valueOf(itemListProduct1.get(position).getId());

                        if (strName == null){
                            Toast.makeText(getContext(), "Name is emty", Toast.LENGTH_SHORT).show();
                        }if (strPrice == null){
                            Toast.makeText(getContext(), "Price is emty", Toast.LENGTH_SHORT).show();
                        }if(strPower == null){
                            Toast.makeText(getContext(), "Power is emty", Toast.LENGTH_SHORT).show();
                        }if (strNumber == null){
                            Toast.makeText(getContext(), "Number is emty", Toast.LENGTH_SHORT).show();
                        }else {
                            final StringRequest request = new StringRequest(Request.Method.POST,
                                    urlEdit,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            switch (response) {
                                                case "403":
                                                    Toast.makeText(getContext(), "no data", Toast.LENGTH_SHORT).show();
                                                    break;
                                                case "400":
                                                    Toast.makeText(getContext(), " error", Toast.LENGTH_SHORT).show();
                                                    break;
                                                case "200":
                                                    Toast.makeText(getContext(), "edit product successfully", Toast.LENGTH_SHORT).show();
                                                    break;
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getContext(), error.getMessage().toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                            ) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    Map<String, String> params = new HashMap<>();
                                    params.put("id_product",id);
                                    params.put("name_product",strName);
                                    params.put("price",strPrice);
                                    params.put("power_consumption",strPower);
                                    params.put("number",strNumber);
                                    return params;
                                }
                            };
                            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                            requestQueue.add(request);
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            case R.id.delete_prodluct:
                final String id = String.valueOf(itemListProduct1.get(position).getId());

                final StringRequest request = new StringRequest(Request.Method.POST,
                        urlDelete,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                switch (response) {
                                    case "403":
                                        Toast.makeText(getContext(), "no data", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "400":
                                        Toast.makeText(getContext(), " error", Toast.LENGTH_SHORT).show();
                                        break;
                                    case "200":
                                        Toast.makeText(getContext(), "delete product successfully", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), error.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    }
                }
                ) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("id_product",id);
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(request);

                break;
        }
        return super.onContextItemSelected(item);
    }
}
