package com.example.managerenergyhome;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    EditText edtEmail,edtPassword;
    TextView tvCreateAccount;
    CheckBox checkBox;
    String strName, strPass;
    SharedPreferences sharedPreferences;
    String url = "http://10.0.2.2/asm/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mapping();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Login();
            }
        });
        saveInformation();
        tvCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

    }
    public void mapping(){
        btnLogin = findViewById(R.id.btnLogin);
        edtEmail = findViewById(R.id.edtEmailLogin);
        edtPassword = findViewById(R.id.edtPassLogin);
        tvCreateAccount = findViewById(R.id.tvToRegister);
    }
    public void saveInformation(){
        checkBox = findViewById(R.id.checkboxRememberMe);
        sharedPreferences = getSharedPreferences("myfile", Context.MODE_PRIVATE);
        Boolean isSave = sharedPreferences.getBoolean("save_information", false);
        if(isSave){
            edtEmail.setText(sharedPreferences.getString("email",""));
            edtPassword.setText(sharedPreferences.getString("pass",""));
            checkBox.setChecked(true);
        }
    }
    public void Login(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        String regex = "^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$";
        if (edtEmail.getText().toString().equals(""))
        {
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show();
        }else if(!edtEmail.getText().toString().trim().matches(regex))
        {
            Toast.makeText(this, "Email is wrong", Toast.LENGTH_SHORT).show();
        }
        else if (edtPassword.getText().toString().equals(""))
        {
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show();
        }else  if(edtPassword.getText().toString().trim().length() < 6 || edtPassword.getText().toString().trim().length() > 16)
        {
            Toast.makeText(this, "Username length is from 6 to 16 characters", Toast.LENGTH_SHORT).show();
        }
        else{
            progressDialog.show();
            strName = edtEmail.getText().toString().trim();
            strPass = edtPassword.getText().toString().trim();
            final StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    switch (response) {
                        case "403":
                            Toast.makeText(LoginActivity.this, "no data", Toast.LENGTH_SHORT).show();
                            break;
                        case "400":
                            Toast.makeText(LoginActivity.this, " error", Toast.LENGTH_SHORT).show();
                            break;
                        case "200":
                            Toast.makeText(LoginActivity.this, "register successfully", Toast.LENGTH_SHORT).show();
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            if(checkBox.isChecked()){
                                editor.putString("email",strName);
                                editor.putString("pass",strPass);
                            }
                            editor.putBoolean("save_information",true);
                            editor.commit();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("email",strName);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                            break;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(LoginActivity.this, error.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    Log.d("register", error.getMessage().toString());
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("email",strName);
                    params.put("pass",strPass);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
            requestQueue.add(request);
        }
    }
}