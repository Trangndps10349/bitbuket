package com.example.managerenergyhome.model;

public class User {
    private int id;
    private String username;
    private String password;
    private String email;
    private String create_date;

    public User() {
    }

    public User(int id, String username, String password, String email, String create_date) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.create_date = create_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", create_date='" + create_date + '\'' +
                '}';
    }
}
